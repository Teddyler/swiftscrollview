//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Tyler Barnes on 1/20/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let scrollview = UIScrollView(frame:self.view.bounds)
        scrollview.backgroundColor = UIColor.white
        self.view.addSubview(scrollview)
        scrollview.isPagingEnabled = true
        scrollview.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        
        let whiteView = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        whiteView.backgroundColor = UIColor.white
        whiteView.text = "1"
        whiteView.textAlignment = NSTextAlignment.center
        whiteView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(whiteView)
        
        let blackView = UILabel(frame: CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        blackView.backgroundColor = UIColor.black
        blackView.text = "2"
        blackView.textAlignment = NSTextAlignment.center
        blackView.font = UIFont.systemFont(ofSize: 300)
        blackView.textColor = UIColor.white
        scrollview.addSubview(blackView)
        
        let blueView = UILabel(frame: CGRect(x: self.view.frame.size.width * 2, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        blueView.backgroundColor = UIColor.blue
        blueView.text = "3"
        blueView.textAlignment = NSTextAlignment.center
        blueView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(blueView)
        
        let brownView = UILabel(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        brownView.backgroundColor = UIColor.brown
        brownView.text = "4"
        brownView.textAlignment = NSTextAlignment.center
        brownView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(brownView)
        
        let yellowView = UILabel(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        yellowView.backgroundColor = UIColor.yellow
        yellowView.text = "5"
        yellowView.textAlignment = NSTextAlignment.center
        yellowView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(yellowView)
        
        let cyanView = UILabel(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        cyanView.backgroundColor = UIColor.cyan
        cyanView.text = "6"
        cyanView.textAlignment = NSTextAlignment.center
        cyanView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(cyanView)
        
        let darkGrayView = UILabel(frame: CGRect(x: 0, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        darkGrayView.backgroundColor = UIColor.darkGray
        darkGrayView.text = "7"
        darkGrayView.textAlignment = NSTextAlignment.center
        darkGrayView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(darkGrayView)
        
        let greenView = UILabel(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        greenView.backgroundColor = UIColor.green
        greenView.text = "8"
        greenView.textAlignment = NSTextAlignment.center
        greenView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(greenView)
        
        let purpleView = UILabel(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        purpleView.backgroundColor = UIColor.purple
        purpleView.text = "9"
        purpleView.textAlignment = NSTextAlignment.center
        purpleView.font = UIFont.systemFont(ofSize: 300)
        scrollview.addSubview(purpleView)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

